package com.moviemanagment.Dto;


public class MovieDto {
	
	private int movieID;
	private String movieName;
	private String movieDesc;
	private String movieShowTime;
	private String seats;
	private String avSeats;
	
	//Create Constructor
	public MovieDto(int movieID, String movieName, String movieDesc, String movieShowTime, String seats) {
		// TODO Auto-generated constructor stub
		this.movieID = movieID;
		this.movieName = movieName;
		this.movieDesc = movieDesc;
		this.movieShowTime = movieShowTime;
		this.seats = seats;
		
	}
	
	public int getMovieID() {
		return movieID;
	}
	public void setMovieID(int movieID) {
		this.movieID = movieID;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieDesc() {
		return movieDesc;
	}
	public void setMovieDesc(String movieDesc) {
		this.movieDesc = movieDesc;
	}
	public String getMovieShowTime() {
		return movieShowTime;
	}
	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}

	public String getAvSeats() {
		return avSeats;
	}

	public void setAvSeats(String avSeats) {
		this.avSeats = avSeats;
	}
	
	
	
	

}
