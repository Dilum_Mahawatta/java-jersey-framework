package com.moviemanagment.Dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.bookingmanagment.Dto.BookDto;
import com.moviemanagment.Dto.MovieDto;

public class MovieDao {

	private String JDBC_URL = "jdbc:mysql://localhost:3306/moviedb?useSSL=false";
	private String JSBC_USERNAME = "root";
	private String JDBC_PASSWORD = "root";

	private static final String SELECT_ALL_MOVIE_DETAILS = "select * from movie";
	private static final String INSERT_BOOKING = "INSERT INTO booking (movieID, seats, movieShowTime) VALUES (?, ?, ?)";
	private static final String UPDATE_SEATS = "UPDATE movie SET seats=? WHERE movieID=?";

	// SQL Connection
	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(JDBC_URL, JSBC_USERNAME, JDBC_PASSWORD);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return connection;
	}

	// Select All Movies
	public List<MovieDto> selectAllMovies() {

		List<MovieDto> movies = new ArrayList<>();
		// Step 1: Establishing a Connection
		try (Connection connection = getConnection();

				// Step 2:Create a statement using connection object
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_MOVIE_DETAILS);) {
			System.out.println(preparedStatement);
			// Step 3: Execute the query or update query
			ResultSet rs = preparedStatement.executeQuery();

			// Step 4: Process the ResultSet object.
			while (rs.next()) {
				int movieID = rs.getInt("movieID");
				String movieName = rs.getString("movieName");
				String movieDesc = rs.getString("movieDesc");
				String movieShowTime = rs.getString("movieShowTime");
				String seats = rs.getString("seats");
				System.out.println(movieID + " " + movieName + " " + movieDesc + " " + movieShowTime + " " + seats);
				movies.add(new MovieDto(movieID, movieName, movieDesc, movieShowTime, seats));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return movies;
	}

	// Book Movie
	public void bookMovie(BookDto bookMovie, int avSeats, int seats) {
		// TODO Auto-generated method stub
		System.out.println(INSERT_BOOKING);

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_BOOKING)) {
			preparedStatement.setString(1, bookMovie.getMovieID());
			preparedStatement.setString(2, bookMovie.getSeats());
			preparedStatement.setString(3, bookMovie.getMovieShowTime());
			System.out.println(preparedStatement);
			int flag = preparedStatement.executeUpdate();

			if (flag > 0) {
				int Available_seats = avSeats-seats;
				System.out.println(seats + " " + avSeats + " " + Available_seats);
				updateSeats(Available_seats, bookMovie);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateSeats(int available_seats, BookDto bookMovie) {
		// TODO Auto-generated method stub
		System.out.println(UPDATE_SEATS);

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SEATS)) {
			preparedStatement.setInt(1, available_seats);
			preparedStatement.setString(2, bookMovie.getMovieID());
			System.out.println(preparedStatement);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
