package com.moviemanagment.Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bookingmanagment.Dto.BookDto;
import com.moviemanagment.Dao.MovieDao;
import com.moviemanagment.Dto.MovieDto;

/**
 * Servlet implementation class MovieServlet
 */
@WebServlet("/")
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MovieDao movieDao;

	/**
	 * Default constructor.
	 */
	public MovieServlet() {
		// TODO Auto-generated constructor stub
		this.movieDao = new MovieDao();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String action = request.getServletPath();

		try {
			switch (action) {

			case "/book":
				bookMovie(request, response);
				break;

			default:
				listMovie(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}

	}

	private void listMovie(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List<MovieDto> listMovie = movieDao.selectAllMovies();
		request.setAttribute("listMovie", listMovie);
		RequestDispatcher dispatcher = request.getRequestDispatcher("movie-list.jsp");
		dispatcher.forward(request, response);
	}

	private void bookMovie(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		String movieID = request.getParameter("movieID");
		String seats = request.getParameter("seats");
		String movieShowTime = request.getParameter("time");
		String avSeats = request.getParameter("avSeats");
		int avSeats2 = Integer.parseInt(avSeats);
		int seats2 = Integer.parseInt(seats);

		if (avSeats2 < seats2) {
			response.sendRedirect("error-ui.jsp");
		} else {
			BookDto bookMovie = new BookDto(movieID, seats, movieShowTime);
			movieDao.bookMovie(bookMovie, avSeats2, seats2);
			response.sendRedirect("message-ui.jsp");
		}

	}

}
