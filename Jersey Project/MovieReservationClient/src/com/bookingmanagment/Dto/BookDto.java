package com.bookingmanagment.Dto;

public class BookDto {

	private int bookingID;
	private String userID;
	private String movieID;
	private String seats;
	private String movieShowTime;

	public BookDto(String movieID, String seats, String movieShowTime) {
		// TODO Auto-generated constructor stub
		this.movieID = movieID;
		this.movieShowTime = movieShowTime;
		this.seats = seats;
	}

	public int getBookingID() {
		return bookingID;
	}

	public void setBookingID(int bookingID) {
		this.bookingID = bookingID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getMovieID() {
		return movieID;
	}

	public void setMovieID(String movieID) {
		this.movieID = movieID;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

	public String getMovieShowTime() {
		return movieShowTime;
	}

	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}

}
