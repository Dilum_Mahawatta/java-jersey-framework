<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Movie Reservation Application</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
<div class="jumbotron text-center">
		<center>
			<h1>Movie Reservations Management</h1>

		</center>
		
		<div class="container">
		<form class="form-horizontal" action="list">
		<h2 align="center">Booking Success</h2>
		<div class="form-group" align="center">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="submit" class="btn btn-default">Back to List</button>
			</div>
		</div>
		</form>
		</div>
	</div>
</body>
</html>