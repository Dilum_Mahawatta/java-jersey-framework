<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Movie Reservation Application</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<div class="jumbotron text-center">

		<h1>Movie Reservations Management</h1>
	

	</div>
	<form class="form-horizontal" action="book">
		<div class="form-group" hidden="">
			<label class="control-label col-sm-2" >Movie ID:</label>
			<div class="col-sm-5">
				<input type="text" name="movieID" class="form-control"
					value="<%=request.getParameter("movieID")%>">
			</div>
		</div>
		<div class="form-group" hidden="">
			<label class="control-label col-sm-2" >Available Seats:</label>
			<div class="col-sm-5">
				<input type="text" name="avSeats" class="form-control"
					value="<%=request.getParameter("avSeats")%>">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2">No Of Seats:</label>
			<div class="col-sm-5">
				<input type="text" name="seats" class="form-control"
					placeholder="Enter No Of Seats" required="required">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" >Time:</label>
			<div class="col-sm-5">
				<input type="text" name="time" class="form-control"
					placeholder="Enter Time" required="required">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Submit</button>
			</div>
		</div>
		
	</form>
</body>
</html>
