<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Movie Reservation Application</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
	<div class="jumbotron text-center">
		<center>
			<h1>Movie Reservations Management</h1>

		</center>
	</div>
	<div class="container">
		<table class="table">
			<caption>
				<h2 align="center">List of Available Movies</h2>
			</caption>
			<tr>
				<th>Movie ID</th>
				<th>Movie Name</th>
				<th>Movie Description</th>
				<th>Movie Show Time</th>
				<th>Available Seats</th>
				<th>Action</th>
			</tr>
			<c:forEach var="movie" items="${listMovie}">
				<tr>
					<td><c:out value="${movie.movieID}" /></td>
					<td><c:out value="${movie.movieName}" /></td>
					<td><c:out value="${movie.movieDesc}" /></td>
					<td><c:out value="${movie.movieShowTime}" /></td>
					<td><c:out value="${movie.seats}" /></td>
					<td><a href="book-movie.jsp?movieID=${movie.movieID}&avSeats=${movie.seats}"<c:out value='${movie.movieID}' />>Book</a>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>

