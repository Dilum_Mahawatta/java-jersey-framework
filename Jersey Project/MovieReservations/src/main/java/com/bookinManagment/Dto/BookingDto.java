package com.bookinManagment.Dto;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


import com.movieManagment.Dto.CriteriaMovie;

@Entity
@Table(name = "booking")
@Access(value = AccessType.FIELD)
public class BookingDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Declare Variables
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookingID;
	
	private String movieID;
	@Column
	private String userID;
	@Column
	private int seats;
	@Column
	private String movieShowTime;
	@Column
	private int rSeats;
	
	@ManyToOne
	@JoinColumn(name="movieID",insertable = false, updatable = false)
	private CriteriaMovie CriteriaMovie;
	
	
	
	public CriteriaMovie getCriteriaMovie() {
		return CriteriaMovie;
	}

	public void setCriteriaMovie(CriteriaMovie criteriaMovie) {
		CriteriaMovie = criteriaMovie;
	}

	//Declare Getters and Setters
	public int getBookingID() {
		return bookingID;
	}

	public void setBookingID(int bookingID) {
		this.bookingID = bookingID;
	}

	public int getrSeats() {
		return rSeats;
	}
	public void setrSeats(int rSeats) {
		this.rSeats = rSeats;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getMovieID() {
		return movieID;
	}
	public void setMovieID(String movieID) {
		this.movieID = movieID;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public String getMovieShowTime() {
		return movieShowTime;
	}
	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}
	
	
}
