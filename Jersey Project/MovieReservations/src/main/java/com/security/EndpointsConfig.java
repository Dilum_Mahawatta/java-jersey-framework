package com.security;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.movieManagment.Controller.CriteriaController;
import com.movieManagment.Controller.MovieController;


public class EndpointsConfig extends Application {

	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();

	public EndpointsConfig() {
		classes.add(MovieController.class);
		classes.add(CriteriaController.class);

	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}
}
