package com.movieManagment.Controller;

import java.util.List;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.movieManagment.Dto.CriteriaDto;
import com.movieManagment.Dto.MovieDto;
import com.movieManagment.Service.CriteriaService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;



@Path("/rest/moviesCritirea")
@Api(value="/rest/moviesCritirea",description="REST Endpoints for Movie Service Critirea Builder")
//@SwaggerDefinition(tags= {@Tag(name="Critirea Query", description="REST Endpoints for Movie Service Critirea Builder")})
public class CriteriaController {

	// Create Object for Service Class
	CriteriaService service = new CriteriaService();

	// Get Movie Details All By ID
	// @RolesAllowed("ADMIN")
	@POST
	@Path("/getAll/{ID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="message print",response=String.class)
	public List<MovieDto> searchAllMovieDetails(@PathParam("ID") String ID, @QueryParam("sorting") String sortingValues,
			@QueryParam("index") String indexValues, @QueryParam("count") String countValue, CriteriaDto movie) {
		System.out.println(" | Movie Controller Get By ID Method |" + sortingValues);
		System.out.println(ID + " " + indexValues + " " + countValue + " " + movie.getSearchparms().getMovieName() + " "
				+ movie.getSearchparms().getLeaseEndDates().getLeaseEndDateFrom());
		
		String sortString = sortingValues.toLowerCase(Locale.ROOT);
		System.out.println("LOWER CASE :"+sortString);
		return service.searchAllMovieDetails(ID, movie, sortingValues, countValue, indexValues);
	}

}
