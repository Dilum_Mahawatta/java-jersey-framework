package com.movieManagment.Controller;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.bookinManagment.Dto.BookingDto;
import com.movieManagment.Dto.CriteriaDto;
import com.movieManagment.Dto.CriteriaMovie;
import com.movieManagment.Dto.MovieDto;
import com.movieManagment.Dto.MovieResponce;
import com.movieManagment.Service.MovieService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;



@Path("rest/movies")
@Api(value="rest/movies",description="REST Endpoints for Movie Service")
//@SwaggerDefinition(tags= {@Tag(name="Movie Service", description="REST Endpoints for Movie Service")})
public class MovieController {

	// Create Object for Service Class
	MovieService service = new MovieService();

	// Add Movie Details
	@POST
	@Path("/create")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="message print",response=String.class)
	public MovieResponce addMovieDetails(MovieDto movie) {
		System.out.println("| Movie Controller addMovieDetails Method | ");
		// System.out.println(movie.getMovieName());

		return service.addMovieDetails(movie);
	}

	// Add Booking Details
	@POST
	@Path("/booking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="message print",response=String.class)
	public MovieResponce addBookingDetails(BookingDto book) {
		System.out.println("| Movie Controller addBookingDetails Method | ");
		System.out.println(book.getMovieID());

		return service.addBookingDetails(book);
	}

	// Delete Movie Details
	@DELETE
	@Path("/delete/{movieID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="message print",response=String.class)
	public MovieResponce deleteMovieDetails(@PathParam("movieID") int movieID) {
		System.out.println(" | Movie Controller deleteMoviedetails Methid |");
		System.out.println(movieID);

		return service.deleteMovieDetails(movieID);

	}

	// Get Movie Details With Join
	//@RolesAllowed("ADMIN")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value="message print",response=String.class)
	public List<MovieDto> getMovieDetails() {
		System.out.println(" | Movie Controller getMovieDetails Method |");

		return service.getMovieDetails();
	}

	// Update Movie Details
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/update/{movieID}")
	@ApiOperation(value="message print",response=String.class)
	public MovieResponce updateMovie(@PathParam("movieID") int movieID, MovieDto movie) {
		System.out.println(" | Movie Controller updateMovie Method |");

		return service.updateMovie(movieID, movie);
	}

	// Get Movie Details All By Time
	// @RolesAllowed("ADMIN")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getmovie/{time}")
	@ApiOperation(value="message print",response=String.class)
	public List<MovieDto> getAllMovieDetails(@PathParam("time") String time) {
		System.out.println(" | Movie Controller Get By ID Method |");
		System.out.println(time);
		return service.getAllMovieDetails(time);
	}


}
