package com.movieManagment.Dto;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonProperty;


public class SearchParms {
	
	@JsonProperty(value = "movieName")
	private String movieName;
	@JsonProperty(value = "movieDesc")
	private String movieDesc;
	@JsonProperty(value = "movieShowTime")
	private String movieShowTime;
	@JsonProperty(value = "seats")
	private int seats;
	@JsonProperty(value = "leaseEndDates")
	private LeaseEndDates leaseEndDates;
	
	
	public LeaseEndDates getLeaseEndDates() {
		return leaseEndDates;
	}
	public void setLeaseEndDates(LeaseEndDates leaseEndDates) {
		this.leaseEndDates = leaseEndDates;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieDesc() {
		return movieDesc;
	}
	public void setMovieDesc(String movieDesc) {
		this.movieDesc = movieDesc;
	}
	public String getMovieShowTime() {
		return movieShowTime;
	}
	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	

}
