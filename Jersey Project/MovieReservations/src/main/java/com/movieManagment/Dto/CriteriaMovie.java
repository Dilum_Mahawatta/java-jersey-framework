package com.movieManagment.Dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.bookinManagment.Dto.BookingDto;

@Entity
@Table(name = "moviecriteria")
@Access(value= AccessType.FIELD)
public class CriteriaMovie implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int movieID;
	
	private String movieName;
	private String movieDesc;
	private String movieShowTime;
	private int seats;
	private String leaseEndDateFrom;
	private String leaseEndDateTo;
	

	public String getLeaseEndDateFrom() {
		return leaseEndDateFrom;
	}
	public void setLeaseEndDateFrom(String leaseEndDateFrom) {
		this.leaseEndDateFrom = leaseEndDateFrom;
	}
	public String getLeaseEndDateTo() {
		return leaseEndDateTo;
	}
	public void setLeaseEndDateTo(String leaseEndDateTo) {
		this.leaseEndDateTo = leaseEndDateTo;
	}
	
	public int getMovieID() {
		return movieID;
	}
	public void setMovieID(int movieID) {
		this.movieID = movieID;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieDesc() {
		return movieDesc;
	}
	public void setMovieDesc(String movieDesc) {
		this.movieDesc = movieDesc;
	}
	public String getMovieShowTime() {
		return movieShowTime;
	}
	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	
}
