package com.movieManagment.Dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class CriteriaDto {
	
	@JsonProperty(value = "movieID")
	private int movieID;
	@JsonProperty(value = "movieName")
	private String movieName;
	@JsonProperty(value = "movieDesc")
	private String movieDesc;
	@JsonProperty(value = "movieShowTime")
	private String movieShowTime;
	@JsonProperty(value = "seats")
	private int seats;
	@JsonProperty(value = "searchparms")
	private SearchParms searchparms;
	
	public int getMovieID() {
		return movieID;
	}
	public void setMovieID(int movieID) {
		this.movieID = movieID;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieDesc() {
		return movieDesc;
	}
	public void setMovieDesc(String movieDesc) {
		this.movieDesc = movieDesc;
	}
	public String getMovieShowTime() {
		return movieShowTime;
	}
	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	public SearchParms getSearchparms() {
		return searchparms;
	}
	public void setSearchparms(SearchParms searchparms) {
		this.searchparms = searchparms;
	}
	
	

}
