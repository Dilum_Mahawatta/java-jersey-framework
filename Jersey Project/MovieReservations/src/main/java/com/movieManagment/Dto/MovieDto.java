package com.movieManagment.Dto;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "movie")
@Access(value= AccessType.FIELD)
public class MovieDto {

	//Declare Variables
	@Id
	@Column
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int movieID;
	@Column
	private String movieName;
	@Column
	private String movieDesc;
	@Column
	private String movieShowTime;
	@Column
	private int seats;
	
	
	//Declare Getters and Setters
	public int getMovieID() {
		return movieID;
	}
	public void setMovieID(int movieID) {
		this.movieID = movieID;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getMovieDesc() {
		return movieDesc;
	}
	public void setMovieDesc(String movieDesc) {
		this.movieDesc = movieDesc;
	}
	public String getMovieShowTime() {
		return movieShowTime;
	}
	public void setMovieShowTime(String movieShowTime) {
		this.movieShowTime = movieShowTime;
	}
	public int getSeats() {
		return seats;
	}
	public void setSeats(int seats) {
		this.seats = seats;
	}
	
	
	
	
	
	
}
