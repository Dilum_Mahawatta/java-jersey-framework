package com.movieManagment.Dto;

import java.util.Map;

public class MovieResponce {

	private Map<String, Object> responseMap;

	public Map<String, Object> getResponseMap() {
		return responseMap;
	}

	public void setResponseMap(Map<String, Object> responseMap) {
		this.responseMap = responseMap;
	}

}
