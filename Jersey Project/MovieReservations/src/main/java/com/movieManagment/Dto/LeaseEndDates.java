package com.movieManagment.Dto;

import org.codehaus.jackson.annotate.JsonProperty;

public class LeaseEndDates {

	@JsonProperty(value = "leaseEndDateFrom")
	private String leaseEndDateFrom;
	@JsonProperty(value = "leaseEndDateTo")
	private String leaseEndDateTo;
	
	
	public String getLeaseEndDateFrom() {
		return leaseEndDateFrom;
	}
	public void setLeaseEndDateFrom(String leaseEndDateFrom) {
		this.leaseEndDateFrom = leaseEndDateFrom;
	}
	public String getLeaseEndDateTo() {
		return leaseEndDateTo;
	}
	public void setLeaseEndDateTo(String leaseEndDateTo) {
		this.leaseEndDateTo = leaseEndDateTo;
	}
	
	
	
	
	
}
