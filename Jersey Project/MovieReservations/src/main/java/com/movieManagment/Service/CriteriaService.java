package com.movieManagment.Service;

import java.util.List;

import com.movieManagment.Dao.CriteriaDao;
import com.movieManagment.Dto.CriteriaDto;
import com.movieManagment.Dto.MovieDto;



public class CriteriaService {
	
	private static final int DEFAULT_RESULT_COUNT = 3;
    private static final int MAX_RESULT_COUNT = 5;
    private static final int DEFAULT_INDEX = 0;
    
    CriteriaDao critriaDao = new CriteriaDao();
    
    public List<MovieDto> searchAllMovieDetails(String ID,CriteriaDto movie, String sortingValues, String countValue, String indexValues) {
		// TODO Auto-generated method stub
		System.out.println(" | Get Movie Details By ID in Service Layer |");
		
		   int index = DEFAULT_INDEX;
	        if (indexValues != null && !indexValues.isEmpty()) {

	            try {
	                index = Integer.valueOf(indexValues).intValue();
	            } catch (NumberFormatException e) {
	                e.printStackTrace();
	            }
	        }

	        int count = DEFAULT_RESULT_COUNT;
	        if (countValue != null && !countValue.isEmpty()) {

	            try {
	                count = Integer.valueOf(countValue).intValue();
	            } catch (NumberFormatException e) {
	               e.printStackTrace();
	            }

	            if (count >= MAX_RESULT_COUNT) {
	                count = MAX_RESULT_COUNT;
	            }
	        }

		
		List movies = critriaDao.searchAllMovieDetails(ID,movie,sortingValues,index,count);
		return movies;
	}

}
