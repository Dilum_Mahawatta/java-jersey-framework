package com.movieManagment.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.bookinManagment.Dto.BookingDto;
import com.movieManagment.Dao.MovieDao;
import com.movieManagment.Dto.MovieDto;
import com.movieManagment.Dto.MovieResponce;

public class MovieService {
	
	

	// Create Object for Database Layer
	MovieDao movieDao = new MovieDao();
	

	// Declare Response Map for Return
	MovieResponce response = new MovieResponce();
	Map<String, Object> responseMap = new HashMap<String, Object>();
	int flag = 0;

	// Add Movie Details Mthod
	public MovieResponce addMovieDetails(MovieDto movie) {
		// TODO Auto-generated method stub
		System.out.println("| addMovieDetails in Service Layer");

		if (movie.getSeats() > 10) {
			responseMap.put("Message", "Maximum Seats are 10");
		} else {
			flag = movieDao.addMovieDetails(movie);

			if (flag > 0) {
				responseMap.put("Message", "Successfully Inserted");
			} else {
				responseMap.put("Message", "Unsuccessfull");
			}
		}

		response.setResponseMap(responseMap);
		return response;
	}

	// Delete Movie Details
	public MovieResponce deleteMovieDetails(int movieID) {
		// TODO Auto-generated method stub
		System.out.println(" | Delete Movie Details in Service Layer |");

		// flag = movieDao.getMovieDetails();
		flag = movieDao.deleteMovieDetails(movieID);

		if (flag == 1) {
			responseMap.put("Message", "Successfully Deleted");
		} else if (flag == 2) {
			responseMap.put("Message", "Booking Movie, Can not Delete");
		} else {
			responseMap.put("Message", "Unsuccessfull");
		}

		response.setResponseMap(responseMap);
		return response;

	}

	// Get Movie Details Method
	public List<MovieDto> getMovieDetails() {
		// TODO Auto-generated method stub
		System.out.println(" | Get Movie Details in Service Layer |");

		List movies = movieDao.getMovieDetails();
		return movies;
	}

	// Update Movie Details Method
	public MovieResponce updateMovie(int movieID, MovieDto movie) {
		// TODO Auto-generated method stub
		System.out.println(" | Update Movie Details in Service Layer |");

		flag = movieDao.updateMovie(movieID, movie);

		if (flag > 0) {
			responseMap.put("Message", "Successfully Updated");
		} else {
			responseMap.put("Message", "Unsuccessfull");
		}

		response.setResponseMap(responseMap);
		return response;
	}

	// Get Movie Details By Time
	public List<MovieDto> getAllMovieDetails(String time) {
		// TODO Auto-generated method stub
		System.out.println(" | Get Movie Details By ID in Service Layer |");
		String RealTime = null;
		
		if (time.contentEquals("10.3")) {
			RealTime = time+"0 pm";
		}else if(time.contentEquals("8.3")) {
			RealTime = time+"0 pm";
		}else {
			RealTime = time+"0 am";
		}
		
		List movies = movieDao.getAllMovieDetails(RealTime);
		return movies;
	}

	// Add Booking Details
	public MovieResponce addBookingDetails(BookingDto book) {
		// TODO Auto-generated method stub
		System.out.println("| addBookingDetails in Service Layer");

		if (book.getrSeats() < book.getSeats()) {
			responseMap.put("Message", "Can not book - Exceed Seats");
		} else {
			
			String RealTime = null;
			
			if (book.getMovieShowTime().contentEquals("10.3")) {
				RealTime = book.getMovieShowTime()+"0 pm";
			}else if(book.getMovieShowTime().contentEquals("8.3")) {
				RealTime = book.getMovieShowTime()+"0 pm";
			}else {
				RealTime = book.getMovieShowTime()+"0 am";
			}
			
			flag = movieDao.addBookingDetails(book,RealTime);

			if (flag > 0) {
				responseMap.put("Message", "Successfully Booked");
			} else {
				responseMap.put("Message", "Unsuccessfull");
			}
		}

		response.setResponseMap(responseMap);
		return response;
	}

	

	

}
