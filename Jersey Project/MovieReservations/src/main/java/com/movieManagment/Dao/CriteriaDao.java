package com.movieManagment.Dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;

import com.bookinManagment.Dto.BookingDto;
import com.movieManagment.*;
import com.movieManagment.Dto.CriteriaDto;
import com.utill.TypedQueryBuilder;

public class CriteriaDao {

	// Entity Manager
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("movie_catalog");
	EntityManager em = emf.createEntityManager();

	public List searchAllMovieDetails(String ID, CriteriaDto movie, String sortingValues, int index, int count) {
		System.out.println(" | Get Movie Details By ID in Dao Layer | " + em);
		TypedQueryBuilder queryBuilder = new TypedQueryBuilder();
		List<Object> results = null;

		// Retrieve the count
		Long countResult = 0L;
		try {
			TypedQuery<Long> query = em.createQuery(queryBuilder.getCount(em));
			countResult = query.getSingleResult();
			System.out.println("Count Results : " + countResult);

		} catch (HibernateException e) {
			e.printStackTrace();
		}

		// Validate the index
		System.out.println(count + " " + index);
		int firstResult = index;
		if (Long.valueOf(firstResult).compareTo(countResult) >= 0) {
			firstResult = 0;
		}

		if (sortingValues == null || sortingValues.isEmpty()) {
			sortingValues = "oldest";
		}

		System.out.println("first Results : " + firstResult);
		try {
			System.out.println("Inside the try catch");

			TypedQuery<Object> query = em.createQuery(queryBuilder.getResult(ID, movie, sortingValues, em));
			System.out.println("SET INDEX :" + firstResult + " " + " SET COUNT : " + count);
			query.setMaxResults(count);
			query.setFirstResult(firstResult);
			results = query.getResultList();
			System.out.println("size :"+results.size());

		} catch (HibernateException e) {
			e.printStackTrace();
		}
		
		for(Object objects : results) {
			BookingDto movies = (BookingDto)objects;
			System.out.println(movies.getMovieID());
			System.out.println(movies.getCriteriaMovie().getMovieName());
		}

		return results;
	}

}
