package com.movieManagment.Dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaQuery;


import com.bookinManagment.Dto.BookingDto;
import com.movieManagment.Dto.CriteriaDto;
import com.movieManagment.Dto.CriteriaMovie;
import com.movieManagment.Dto.MovieDto;
import com.utill.SessionUtill;
import com.utill.TypedQueryBuilder;

public class MovieDao {
	
	
	 // Create Hibernet Session
	Session session = SessionUtill.getSession();
	Transaction tx = session.beginTransaction();

	

	// Insert Movie Details
	public int addMovieDetails(MovieDto movie) {
		// TODO Auto-generated method stub
		System.out.println("| Add Movie Details in Dao Layer");
		System.out.println(movie.getMovieDesc() + " " + movie.getMovieName() + " " + movie.getMovieShowTime() + " "
				+ movie.getSeats());
		MovieDto moviedto = new MovieDto();
		int flag = 1;

		moviedto.setMovieName(movie.getMovieName());
		moviedto.setMovieDesc(movie.getMovieDesc());
		moviedto.setMovieShowTime(movie.getMovieShowTime());
		moviedto.setSeats(movie.getSeats());

		session.save(moviedto);

		tx.commit();
		session.close();

		return flag;
	}

	// Delete Movie Details
	public int deleteMovieDetails(int movieID) {
		// TODO Auto-generated method stub
		System.out.println(" | Delete Movie Details in Dao LAyer |");

		int flag = 0;
		Object rowCount = 0;
		try {

			String sql = "select max(b.bookingID) from BookingDto b where b.movieID='" + movieID + "'";
			Query query = session.createQuery(sql);
			System.out.println(query);
			// query.setInteger("mID", movieID);

			rowCount = query.uniqueResult();
			System.out.println("Rows affected: " + rowCount);

			if (rowCount != null) {
				flag = 2;
			} else {
				String Sql = "delete from MovieDto where movieID =:mID";
				Query query2 = session.createQuery(Sql);
				query2.setInteger("mID", movieID);
				flag = query2.executeUpdate();
				System.out.println(" Delete Status " + flag);
			}

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			tx.commit();
			session.close();
		}

		return flag;
	}

	// Get Movie Details
	public List<MovieDto> getMovieDetails() {
		// TODO Auto-generated method stub
		System.out.println(" | Get Movie Details in Dao LAyer |");
		List<MovieDto> movies = new ArrayList<MovieDto>();

		try {

			movies = session.createQuery("SELECT m, SUM(b.seats) " + "FROM MovieDto m, BookingDto b "
					+ "WHERE m.movieID = b.movieID " + "GROUP BY b.movieID").list();

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

		return movies;

	}

	// Update Movie Details Method
	public int updateMovie(int movieID, MovieDto movie) {
		// TODO Auto-generated method stub
		System.out.println(" | Update Movie Details in Dao LAyer |");
		int rowCount = 0;
		try {
			String sql = "update MovieDto set movieName = :mname, movieDesc=:mdesc, movieShowTime=:mtime, "
					+ "seats=:mseats where movieID = :mid";
			Query query = session.createQuery(sql);

			query.setInteger("mid", movieID);
			query.setString("mname", movie.getMovieName());
			query.setString("mdesc", movie.getMovieDesc());
			query.setString("mtime", movie.getMovieShowTime());
			query.setInteger("mseats", movie.getSeats());

			rowCount = query.executeUpdate();
			System.out.println("Rows affected: " + rowCount);

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			tx.commit();
			session.close();
		}
		return rowCount;
	}

	// Get Movie Details By Time
	public List<MovieDto> getAllMovieDetails(String time) {
		// TODO Auto-generated method stub
		System.out.println(" | Get Movie Details By ID in Dao Layer |");
		List<MovieDto> movies = new ArrayList<MovieDto>();
		try {

			movies = session
					.createQuery("SELECT m FROM MovieDto m WHERE m.seats != 0 AND m.movieShowTime='" + time + "' ")
					.list();

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			tx.commit();
			session.close();
		}

		return movies;
	}

	// Add Booking Details
	public int addBookingDetails(BookingDto book, String RealTime) {
		// TODO Auto-generated method stub
		System.out.println("| Add Booking Details in Dao Layer | ");
		System.out.println(
				book.getMovieID() + " " + book.getrSeats() + " " + book.getSeats() + " " + book.getMovieShowTime());
		BookingDto bookdto = new BookingDto();
		int flag = 0;
		int AVAIBALE_SEATS = book.getrSeats() - book.getSeats();

		bookdto.setMovieID(book.getMovieID());
		bookdto.setMovieShowTime(RealTime);
		bookdto.setSeats(book.getSeats());
		bookdto.setrSeats(AVAIBALE_SEATS);

		session.save(bookdto);
		// tx.commit();
		// session.close();

		int count = updateMovieSeats(AVAIBALE_SEATS, book.getMovieID(), RealTime);
		if (count > 0) {
			flag = 1;
		}

		return flag;
	}

	// Update Movie Seats After Insert Booking Details
	private int updateMovieSeats(int AVAIBALE_SEATS, String movieID, String time) {
		// TODO Auto-generated method stub
		System.out.println(" | Update Movie Seats in Dao LAyer |");
		int rowCount = 0;
		try {
			String sql = "update MovieDto set seats=:mseats where movieID = :mid AND movieShowTime =:mTime";
			Query query = session.createQuery(sql);

			query.setString("mid", movieID);
			query.setInteger("mseats", AVAIBALE_SEATS);
			query.setString("mTime", time);

			rowCount = query.executeUpdate();
			System.out.println("Rows affected: " + rowCount);

		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
			// TODO: handle exception
		} finally {
			tx.commit();
			session.close();
		}
		return rowCount;

	}


	
}
