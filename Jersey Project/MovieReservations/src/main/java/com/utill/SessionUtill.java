package com.utill;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionUtill {

	private static SessionUtill instance = new SessionUtill();
	private SessionFactory sessionFactory;
	
	public static SessionUtill getInstance() {
		return instance;
	}
	
	private SessionUtill(){
        Configuration configuration = new Configuration();
        configuration.configure("hibernate.cfg.xml");
                
        sessionFactory = configuration.buildSessionFactory();
    }
    
    public static Session getSession(){
        Session session =  getInstance().sessionFactory.openSession();
        
        return session;
    }
}
