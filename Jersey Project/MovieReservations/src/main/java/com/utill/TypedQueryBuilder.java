package com.utill;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.bookinManagment.Dto.BookingDto;
import com.movieManagment.Dto.CriteriaDto;
import com.movieManagment.Dto.CriteriaMovie;





public class TypedQueryBuilder {

	private CriteriaBuilder cb;
	private CriteriaQuery<Object> cq;
	private Root<CriteriaMovie> root;
	private Root<BookingDto> rootbook; 
	private CriteriaQuery<Long> cqCount;



	// GEt Results Method
	public CriteriaQuery<Object> getResult(String iD, CriteriaDto movie, String sortingValues, EntityManager em) {

		System.out.println("Critirea GET Results ");
		
		cb = em.getCriteriaBuilder();
		cq = cb.createQuery();
		root = cq.from(CriteriaMovie.class);
		rootbook = cq.from(BookingDto.class);
		
		//cq.multiselect(rootbook).where(cb.equal(rootbook.get("CriteriaMovie"),root.get("movieID")));	
		if (sortingValues.contentEquals("oldest")) {
			cq.orderBy(cb.desc(rootbook.get("bookingID")));
							
		} else if(sortingValues.contentEquals("newest")) {
			cq.orderBy(cb.asc(rootbook.get("bookingID")));
		}
		
		/*cq.multiselect(rootbook).where(cb.equal(rootbook.get("CriteriaMovie"),root.get("movieID")),
				cb.equal(root.get("movieShowTime"), movie.getSearchparms().getMovieShowTime()),
				cb.equal(root.get("movieName"), movie.getSearchparms().getMovieName()),
				cb.between(root.<String>get("leaseEndDateTo"),
						movie.getSearchparms().getLeaseEndDates().getLeaseEndDateFrom(),
						movie.getSearchparms().getLeaseEndDates().getLeaseEndDateTo()));
*/
		List<Predicate> predicates = new ArrayList<Predicate>();
		System.out.println("DETAILS: "+movie.getSearchparms().getMovieName());
		if(movie.getSearchparms().getMovieName() == null || movie.getSearchparms().getMovieName().isEmpty()) {
			System.out.println("Movie Name IS Empty and add");
		}
		
		
		if (!(movie.getSearchparms().getMovieShowTime() == null || movie.getSearchparms().getMovieShowTime().isEmpty())) {
			 predicates.add(cb.equal(root.get("movieShowTime"), movie.getSearchparms().getMovieShowTime()));
		}if(!(movie.getSearchparms().getMovieName() == null || movie.getSearchparms().getMovieName().isEmpty())) {
			 predicates.add(cb.equal(root.get("movieName"), movie.getSearchparms().getMovieName()));
		}if(!movie.getSearchparms().getLeaseEndDates().getLeaseEndDateFrom().isEmpty() && !movie.getSearchparms().getLeaseEndDates().getLeaseEndDateTo().isEmpty()) {
			 predicates.add(cb.between(root.<String>get("leaseEndDateTo"),
						movie.getSearchparms().getLeaseEndDates().getLeaseEndDateFrom(),
						movie.getSearchparms().getLeaseEndDates().getLeaseEndDateTo()));
		}
		
		 

		System.out.println("predicates SIZE :" + predicates.size());
		// AND all of the predicates together:
		if (!predicates.isEmpty()) {
		 // cq.multiselect(rootbook).where(cb.equal(rootbook.get("CriteriaMovie"),root.get("movieID")));
			cq.multiselect(rootbook).where(cb.equal(rootbook.get("CriteriaMovie"),root.get("movieID")),
				   cb.and(predicates.toArray(new Predicate[predicates.size()])));
		}
		return cq;

	}

	//Get Count
	public CriteriaQuery<Long> getCount(EntityManager em) {
		// TODO Auto-generated method stub
		System.out.println("Critirea COUNT Results ");
		
		cb = em.getCriteriaBuilder();
		cqCount = cb.createQuery(Long.class);
		root = cqCount.from(CriteriaMovie.class);
		
		cqCount.select(cb.count(root));
		return cqCount;
	}

}
