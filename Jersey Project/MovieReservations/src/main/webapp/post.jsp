<%@page import="com.sun.xml.internal.txw2.Document"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script>
	var movieID = <%=request.getParameter("movieID")%>;
	var rSeats = <%=request.getParameter("rSeats")%>;
	var seats = <%=request.getParameter("seats")%>;
	var movieShowTime = <%=request.getParameter("movieShowTime")%>;
	
	var jsonObjects = {movieID,rSeats,seats,movieShowTime}
	$(document)
	.ready(
			function() {
				$
						.ajax({
							type : 'POST',
							url : '/MovieReservations/rest/movies/booking',
							data: JSON.stringify(jsonObjects),
							dataType : 'json',
							contentType : "application/json",
							cache : false,
							success : function(response) {
								console.log(response.responseMap.Message);
								
								$('#loadHtml')
								.append('<h2>'+response.responseMap.Message+'</h2>');
							}
						});
			});
			
</script>
</head>
<body>
	<div class="jumbotron text-center">
		<center>
			<h1>Movie Reservations Management</h1>

		</center>

		<div class="container">
			<form class="form-horizontal" action="index.jsp" id="">
				<!--  <h2 align="center"  id="loadHtml">Successfully Booked!!</h2>-->
				<div class="form-group" align="center">
					<div id="loadHtml"></div>
					<div class="col-sm-offset-2 col-sm-8">
						<button type="submit" class="btn btn-default">View Time</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>