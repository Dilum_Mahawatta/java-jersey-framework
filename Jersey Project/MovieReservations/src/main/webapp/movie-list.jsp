<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Movie Reservation Application</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script>

var time = <%=request.getParameter("time")%>;
console.log(time);
	$(document)
			.ready(
					function() {
						$
								.ajax({
									type : 'GET',
									url : '/MovieReservations/rest/movies/getAll/'+time+'',
									dataType : 'json',
									cache : false,
									success : function(response) {
										$
												.each(
														response,
														function(m, movie) {
															console
																	.log(movie.movieID);

															$('#loadtable')
																	.append(
																			'<tr><td>'
																					+ movie.movieID
																					+ '</td><td>'
																					+ movie.movieName
																					+ '</td><td>'
																					+ movie.movieDesc
																					+ '</td><td>'
																					+ movie.movieShowTime
																					+ '</td><td>'
																					+ movie.seats
																					+ '</td><td><a href="book-movie.jsp?movieID='
																					+ JSON.stringify(movie.movieID)
																					+ '&avSeats='
																					+ JSON.stringify(movie.seats)
																					+ '&movieShowTime='
																					+ time+'">Book</a>');

														});

									}
								});

					});
</script>
</head>
<body>
	<div class="jumbotron text-center">
		<center>
			<h1>Movie Reservations Management</h1>

		</center>
	</div>
	<div class="container">
		<section id='bidderTable' class='container-fluid'>
		<table class='table'>

			<caption>
				<h2 align="center">List of Available Movies - <%=request.getParameter("time")%></h2>
				<a href="index.jsp">Back</a>
			</caption>
			<tr>
				<th>Movie ID</th>
				<th>Movie Name</th>
				<th>Movie Description</th>
				<th>Movie Show Time</th>
				<th>Available Seats</th>
				<th>Action</th>
			</tr>
			<tbody id='loadtable'>

			</tbody>
		</table>
		</section>
</body>
</html>

